open Containers
let (let+) x f = Lwt.bind x f

let api_key = IO.with_in "./api-key" IO.read_all

let make_headers () =
  let headers = Cohttp.Header.init () in
  Cohttp.Header.add headers "Authorization" (Format.sprintf "Bearer %s" api_key)

let get =
  let uri = Uri.of_string "https://robovinci.xyz/" in
  fun endpoint ->
    let headers = make_headers () in
    Cohttp_lwt_unix.Client.get ~headers (Uri.with_path uri endpoint)

let get_submission_status submission_id =
  let+ _, body =
    get (Format.sprintf "/api/submissions/%d" submission_id) in
  let+ body = Cohttp_lwt.Body.to_string body in
  let result = Yojson.Safe.from_string body in
  let status = Yojson.Safe.Util.member "status" result
               |> Yojson.Safe.Util.to_string
               |> function "SUCCEEDED" -> `SUCCEEDED
                         | "FAILED" -> `FAILED
                         | "PROCESSING" -> `PROCESSING
                         | "QUEUED" -> `QUEUED
                         | status ->
                           failwith ("unknown status " ^ status) in
  let cost = Yojson.Safe.Util.member "cost" result
             |> Yojson.Safe.Util.to_int in
  Lwt.return (status, cost)

let submit_candidate ~problem_id solution =
  let uri = Uri.of_string
              (Format.sprintf
                 "https://robovinci.xyz/api/problems/%d"
                 problem_id) in
  let text = Lang.show_program solution in
  let headers = make_headers () in
  let headers =
    Cohttp.Header.add headers
      "Content-Type" "multipart/form-data; boundary=9aa33de891382f2a"
  in
  let+ _, body = 
    Cohttp_lwt_unix.Client.post
      ~headers
      ~body:(Cohttp_lwt.Body.of_string
               (String.concat "\n"
                  [
                    "--9aa33de891382f2a";
                    "Content-Disposition: form-data; name=\"file\"; filename=\"dynamic\"";
                    "Content-Type: application/octet-stream";
                    "";
                    text;
                    "--9aa33de891382f2a--";
                  ]))
      uri in 
  let+ body = Cohttp_lwt.Body.to_string body in
  let result = Yojson.Safe.from_string body in
  let id = 
    Yojson.Safe.Util.member "submission_id" result
    |> Yojson.Safe.Util.to_int in
  Lwt.return id
