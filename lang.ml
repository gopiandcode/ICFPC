open Containers
type block_id = Base of int
              | Dot0 of block_id
              | Dot1 of block_id
              | Dot2 of block_id
              | Dot3 of block_id
[@@deriving eq, ord]

module BlockMap = Map.Make (struct type t = block_id[@@deriving ord]  end)

type offset_1d = int
type offset_2d = int * int
type color = int * int * int * int
type orientation = Horizontal | Vertical

type move =
  | LineCut of block_id * orientation * offset_1d
  | PointCut of block_id * offset_2d
  | ColorMove of block_id * color
  | SwapMove of block_id * block_id
  | Merge of block_id * block_id

type program = move list 

let rec show_block_id = function
  | Base d -> string_of_int d
  | Dot0 bid -> show_block_id bid ^ ".0"
  | Dot1 bid -> show_block_id bid ^ ".1"
  | Dot2 bid -> show_block_id bid ^ ".2"
  | Dot3 bid -> show_block_id bid ^ ".3"

let show_block blk =
  Format.sprintf "[%s]" (show_block_id blk)

let show_orientation = function
  | Vertical -> "[X]"
  | Horizontal -> "[Y]"

let show_offset_1d offset =
  Format.sprintf "[%d]" offset

let show_offset_2d (x,y) =
  Format.sprintf "[%d,%d]" x y

let show_color (r,g,b,a) =
  Format.sprintf "[%d,%d,%d,%d]" r g b a

let show_move = function
  | LineCut (block, orientation, offset) ->
    Format.sprintf "cut %s %s %s"
      (show_block block)
      (show_orientation orientation)
      (show_offset_1d offset)
  | PointCut (block, point) ->
    Format.sprintf "cut %s %s"
      (show_block block)
      (show_offset_2d point)
  | ColorMove (block, color) ->
    Format.sprintf "color %s %s"
      (show_block block)
      (show_color color)
  | SwapMove (block1, block2) ->
    Format.sprintf "swap %s %s"
      (show_block block1)
      (show_block block2)
  | Merge (block1, block2) ->
    Format.sprintf "merge %s %s"
      (show_block block1)
      (show_block block2)

let show_program program =
  List.map show_move program |> String.concat "\n"
