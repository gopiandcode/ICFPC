[@@@warning "-33-69-34-27-26-32"]
open Containers
let (let+) x f = Lwt.bind x f

type block = {
  x: int; y: int;
  w: int; h: int;
  color: Lang.color;
  sub_blocks: block list;
}

type state = {
  next_id: int;
  blocks: block Lang.BlockMap.t;
}

let intersects (x1,y1,w1,h1) (x2,y2,w2,h2) =
  not (x1+w1<x2 || x2+w2<x1 || y1+h1<y2 || y2+h2<y1)

let contains (x,y,w,h) (px,py) =
  (x<px && px<x+w && y<py && py<y+h)


let ensure b = Option.return_if b ()

let rec filter_subblocks (x,y,w,h) : block -> block option =
  fun block ->
  if not @@ intersects (x,y,w,h) (block.x, block.y, block.w, block.h)
  then None
  else begin
    let nx = x - block.x and ny = y - block.y in
    Some {block with
          sub_blocks = List.filter_map
                         (filter_subblocks (nx,ny,w,h))
                         block.sub_blocks}
  end

let update_dimensions block (x,y,w,h) =
  let open Option in
  let* () = ensure (intersects
                      (block.x, block.y, block.w, block.h)
                      (x,y,w,h)) in
  Some {
    block with
    x; y; w; h;
    sub_blocks=List.filter_map
                 (filter_subblocks (x, y, w, h))
                 block.sub_blocks
  }


let eval state : Lang.move -> state option =
  let open Option in
  function
  | Lang.LineCut (id, orientation, offset) ->
    let* block = Lang.BlockMap.find_opt id state.blocks in
    begin
      let* block_0 =
        let* new_dim = match orientation with
          | Lang.Horizontal ->
            let* _ = ensure (block.y < offset && offset - block.y < block.h) in
            let new_h = offset - block.y in
            Some (block.x, block.y, block.w, new_h)
          | Lang.Vertical ->
            let* _ = ensure (block.x < offset && offset - block.x < block.w) in
            let new_w = offset - block.x in
            Some (block.x, block.y, new_w, block.h) in
        update_dimensions block new_dim in
      let* block_1 =
        let* new_dim = match orientation with
          | Lang.Horizontal ->
            let* _ = ensure (block.y < offset && offset - block.y < block.h) in
            let new_y = offset in
            let new_h = block.y + block.h - offset in
            Some (block.x, new_y, block.w, new_h)
          | Lang.Vertical ->
            let* _ = ensure (block.x < offset && offset - block.x < block.w) in
            let new_x = offset in
            let new_w = block.x + block.w - offset in
            Some (new_x, block.y, new_w, block.h) in
        update_dimensions block new_dim in
      Some {state with blocks =
                         (state.blocks
                          |> Lang.BlockMap.remove id
                          |> Lang.BlockMap.add (Dot0 id) block_0
                          |> Lang.BlockMap.add (Dot1 id) block_1)
           }
    end
  | Lang.PointCut (id, ((px,py) as point)) ->
    let* block = Lang.BlockMap.find_opt id state.blocks in
    let* () = ensure (contains (block.x,block.y,block.w,block.h) point) in
    begin
      let* block_0 =
        let new_dim = 
          let new_h = py - block.y in
          let new_w = px - block.x in
          (block.x, block.y, new_w, new_h) in
        update_dimensions block new_dim in
      let* block_1 =
        let new_dim = 
          let new_h = py - block.y in
          let new_x = px in
          let new_w = block.x + block.w - new_x in
          (new_x, block.y, new_w, new_h) in
        update_dimensions block new_dim in
      let* block_2 =
        let new_dim = 
          let new_x = px and new_y = py in
          let new_h = block.y + block.h - new_y in
          let new_w = block.x + block.w - new_x in
          (new_x, new_y, new_w, new_h) in
        update_dimensions block new_dim in
      let* block_3 =
        let new_dim = 
          let new_w = px - block.x in
          let new_y = py in
          let new_h = block.y + block.h - new_y in
          (block.x, new_y, new_w, new_h) in
        update_dimensions block new_dim in
      Some {state with blocks =
                         (state.blocks
                          |> Lang.BlockMap.remove id
                          |> Lang.BlockMap.add (Dot0 id) block_0
                          |> Lang.BlockMap.add (Dot1 id) block_1
                          |> Lang.BlockMap.add (Dot2 id) block_2
                          |> Lang.BlockMap.add (Dot3 id) block_3
                         )
           }
    end
  | Lang.ColorMove (id, color) ->
    let* block = Lang.BlockMap.find_opt id state.blocks in
    let block = {block with color; sub_blocks=[]} in
    Some {state with blocks =
                       (state.blocks
                        |> Lang.BlockMap.remove id
                        |> Lang.BlockMap.add id block
                       )
         }
  | Lang.SwapMove (id1, id2) ->
    let* block1 = Lang.BlockMap.find_opt id1 state.blocks in
    let* block2 = Lang.BlockMap.find_opt id1 state.blocks in
    let* () = ensure (block1.h = block2.h && block1.w = block2.w) in
    let new_block1 = {block1 with x=block2.x; y=block2.y} in
    let new_block2 = {block2 with x=block1.x; y=block1.y} in
    Some {state with blocks =
                       (state.blocks
                        |> Lang.BlockMap.remove id1
                        |> Lang.BlockMap.remove id2
                        |> Lang.BlockMap.add id1 new_block1
                        |> Lang.BlockMap.add id2 new_block2
                       )
         }
  | Lang.Merge (id1, id2) ->
    let* block1 = Lang.BlockMap.find_opt id1 state.blocks in
    let* block2 = Lang.BlockMap.find_opt id1 state.blocks in
    match () with
    | _ when block1.x = block2.x && block1.w = block2.w ->
      let new_x = block1.x in
      let new_y = Int.min block1.y block2.y in
      let new_w = block1.w in
      let new_h = block1.h + block2.h in
      {block1 with
       x=new_x; y=new_y; h=new_h; w=new_w;
       sub_blocks= List.filter_map filter_subblocks (new_x, new_y, new_w, new_h)
                     (block1.sub_blocks, block2.sub_blocks)
      }

    | _ when block1.y = block2.y && block1.h = block2.h ->
      assert false
    | _ -> None

let () =
  Lwt_main.run @@ begin

    let prog = Lang.[
      LineCut (Base 0, Horizontal, 10);
      Merge (Dot0 (Base 0), Dot1 (Base 0));
      ColorMove (Base 1, (0,0,0,255))
    ] in
    print_endline @@ Lang.show_program prog;
    let+ submission_id = 
      Web.submit_candidate ~problem_id:1
        prog in

    let+ status, cost =
      Web.get_submission_status submission_id in
    Format.printf
      "%s: %d@."
      ([%show: [> `FAILED | `PROCESSING | `QUEUED | `SUCCEEDED ]]
         status)
      cost;
    let+ () = Lwt_unix.sleep 5.0 in
    let+ status, cost =
      Web.get_submission_status submission_id in
    Format.printf
      "%s: %d@."
      ([%show: [> `FAILED | `PROCESSING | `QUEUED | `SUCCEEDED ]]
         status)
      cost;

    Lwt.return ()
  end
